var App = function (options) {
  var $el,
    $now

  // init
  var init = function (opts) {
    cacheDOM();
    render();
  }

  // cacheDOM
  var cacheDOM = function () {
    $el = $('#app')
    $now = $el.find('.now')
  }

  // render
  var render = function () {
      setInterval(function () {
        var now = new Date();
            yyyy = now.getFullYear(),
            mm = now.getMonth(),
            dd = now.getDate(),
            hour = now.getHours(),
            minutes = now.getMinutes(),
            seconds = now.getSeconds(),
            day = now.getDay(),
            dayArray = ['星期日', '星期一','星期二', '星期三','星期四', '星期五','星期六'];

            if (seconds < 10) {
                seconds = '0' + seconds;
            } else if (hour < 10) {
                hour = '0' + hour;
            } else if (minutes < 10) {
                minutes = '0' + minutes;
            }
        $now.text(yyyy + '年' + (mm + 1) + '月' + dd + '日 ' + hour + ':' + minutes + ':' + seconds + ' ' + dayArray[day]);
      },1000);
      
  }

  init.call(this, options)
}

new App
